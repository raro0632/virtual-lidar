def list_files(file_pattern,path,file_count=None):
    """ 
    return list of files matching pattern in path directory
    
    Arguments:
    file_pattern: glob pattern for filename, e.g. 'WLS866-16_2015_04_0*__00_00_00.rtd'
    path: path to directory to search
    file_count: number of files to include from end of list, default 'None' returns all
    
    Output:
    sorted list of files including full path
    
    """
    import os
    import glob
    
    assert os.path.isdir(path)
    if path[-1] is not '/':
        path = path+'/'
    
    sorted_files = sorted(glob.iglob(path + file_pattern)) #, reverse=True)
    if file_count is None:
        return sorted_files
    
    else:
        return (sorted_files[:-file_count-1:-1])[::-1]

def wcFiles_to_NetCDF(file,zvars=None,outfile=None):
    import re
    import pandas as pd
    import xarray as xr
    
    # Read off header size and process some of the header data
    f = open(file, encoding='iso-8859-1', buffering=1)  # thanks Leosphere 
    header1 = f.readline()
    assert 'HeaderSize' in header1 or 'HeaderLength' in header1
    headersize = (int)(re.findall(r'\d+', header1)[0])

    lines = []
    for l in range(headersize):
        lines.append(f.readline())

    try:
        alt = [i for i in lines if 'Altitudes' in i][0]
        hgts = [int(s) for s in re.findall(r'\d+', alt)]
        print(hgts)
    except:
        print('ERROR: Cannot find list of altitudes in file header')
        return None

    f.close()

    # Read deliminated file (may need to do some pre-processing in vi to clean up)
    data = pd.read_csv(file, skiprows=headersize+1, sep='\t', encoding='iso-8859-1', \
                      header=0, parse_dates=True, index_col=0, error_bad_lines=False)
    

    data.index.name='Time'

    dropnames = ['Unnamed','Wiper','Temp']
    for var in data.columns.values.tolist():
        for drop in dropnames:
            if drop in var:
                data.drop(axis=1,labels=[var],inplace=True)

    names = data.columns.values.tolist()

    startidx = len(names)%len(hgts)
    nfields = (int)(len(names)/len(hgts))
    
    if zvars is None:
        seppatterns = [re.compile('[0-9]+m '),re.compile('-[0-9]+')]
        zvars = []
        for v in names[startidx:startidx+nfields]:
            for sep in seppatterns:
                try:
                    zvars.append( v.replace( re.search(sep, v).group(0), '' ) )
                except:
                    continue
                    
        if len(zvars) != nfields:
            print('ERROR: Failure recovering variable names collected at each elevation')
            return None
                        
    
    da = xr.DataArray(data.values[:,startidx::nfields], coords=[data.index.values, hgts], dims=['time', 'height'])
    ds = da.to_dataset(name = zvars[0])
    for v in range(1,len(zvars)):
        ds[zvars[v]] = xr.DataArray(data.values[:,startidx+v::nfields], coords=[data.index.values, hgts], dims=['time', 'height'])
        
    for v in range(startidx):
        ds[names[v]] = xr.DataArray(data.values[:,v], coords=[data.index.values], dims=['time'])

    for line in lines:
        splt = line.split('=')
        if len(splt) > 1:
            ds.attrs[splt[0]]=splt[1][:-1]
        
    if outfile is not None:
        ds.to_netcdf(path=outfile)
        
    return ds

