class virtualLidar:
    """General parent class for vitual lidar instruments and their parameters"""
    
    name = 'Default'
    scantype = 'None'
    RWF_func = 'None'
    
    def __init__(self,**kwargs):
        
        print('Created %s virtual lidar'%self.name)
        
        for key in kwargs.keys():
            try:
                setattr(self, key, kwargs[key])
            except:
                continue
                
        for key, value in self.__dict__.items():
            if key == 'name':
                continue
            if callable(value):
                print(key,":",value.__name__)
            else:
                print(key,':',value)
        
    def __str__(self):
        
        #printstr = "Lidar type name:       %s\n"%(self.name)\
        #         + "Range Gate Resolution: %.1fm\n"%(self.range_gate_res)\
        #         + "Minimum Range Gate:    %.1fm\n"%(self.minrange)\
        #         + "Number of Range Gates: %d"%(self.ngates)

        if callable(self.RWF_func):
            rwf_name = self.RWF_func.__name__
        else:
            rwf_name = self.RWF_func
                        
        printstr = "Lidar type name:       %s\n"%(self.name)\
                 + "Range Gate Resolution: %.1fm\n"%(self.range_gate_res)\
                 + "Minimum Range Gate:    %.1fm\n"%(self.minrange)\
                 + "Number of Range Gates: %d\n"%(self.ngates)\
                 + "RWF/laser type:        %s\n"%(rwf_name)\
                 + "Scan type:             %s"%(self.scantype)
        
        return printstr
    
    def print_all(self):
        """print all attributes of the object"""
        print(self.__dict__)
        
    def set_range_gates(self,mingate,ngates,gate_res,rwf_type,rwf_func=None,**kwargs):
        
        self.minrange = mingate
        self.ngates = ngates
        self.range_gate_res = gate_res
        
        if rwf_func is None:
            rwf_functions = {'none':self.none_RWF,'pulsed':self.pulsed_RWF,'cw':self.continuous_wave_RWF}
            self.RWF_func = rwf_functions[rwf_type]
        
        else:
            self.RWF_func = rwf_func
            
        if self.RWF_func == self.pulsed_RWF:
            if 'tau' in kwargs.keys():
                setattr(self, 'tau', kwargs['tau'])
            if 'taum' in kwargs.keys():
                setattr(self, 'taum', kwargs['taum'])
                
            assert 'tau' in self.__dict__.keys(), 'No FWHM [ns] of pulsed lidar set, add set_range_gates(...,tau=XX) to call'
            assert 'taum' in self.__dict__.keys(), 'No range gate interval [ns] of pulsed lidar set, add set_range_gates(...,taum=XX) to call'
            
            self.RWF_func = lambda z,z0: self.pulsed_RWF(z,z0,self.tau,self.taum)
            
        if self.RWF_func == self.continuous_wave_RWF:
            if 'alpha0' in kwargs.keys():
                setattr(self, 'alpha0', kwargs['alpha0'])
            if 'lambdaw' in kwargs.keys():
                setattr(self, 'lambdaw', kwargs['lambdaw'])
                
            assert 'alpha0' in self.__dict__.keys(), 'No effective telescope radius [m] of CW lidar set, add set_range_gates(...,alpha0=XX) to call'
            assert 'lambdaw' in self.__dict__.keys(), 'No lase wavelength [m] of CW lidar set, add set_range_gates(...,lambdaw=XX) to call'
            
            self.RWF_func = lambda z,z0: self.continuous_wave_RWF(z,z0,self.alpha0,self.lambdaw)

    def get_scan_points(self,refresh=False,**kwargs):
        """Return existing scan points or compute them for lidar scan type"""
        
        if 'scanpoints' in self.__dict__.keys():
            #print('Returning previously computed points.')
            return self.scanpoints
        
        elif 'scantype' in self.__dict__.keys():
            print('Computing scan points from lidar defaults')
            self.set_common_scan(self.scantype)
        
        return None
    
    def get_RWF_points(self,**kwargs):
        """Compute the multiple points along each beam necessary to perform the RWF convolution"""
        
        import numpy as np
        import xarray as xr

        scanpoints = self.get_scan_points()
        
        nBeams = len(scanpoints.BeamIndex)
        pti = np.array([[np.nan,np.nan,np.nan]])
        
        ## TODO: generalize range and selection of nodes used for RWF convolution

#         Rmax = scanpoints.radius.isel(BeamIndex=4,AlongBeam=-1).values
#         Rdelta = scanpoints.radius.isel(BeamIndex=4,AlongBeam=0).values
#         R = np.arange(0,Rmax+Rdelta,1)
        
        Rmax = scanpoints.radius.max().values
        Rdelta = scanpoints.radius.min().values
        R = np.arange(4,Rmax+Rdelta,1) ## TODO: make surface disclusion more general?

        for b in range(nBeams):
            elev_rad = np.deg2rad(scanpoints.elev.isel(BeamIndex=b).values)
            az_rad = np.deg2rad(scanpoints.az.isel(BeamIndex=b).values)

            z = R*np.sin(elev_rad)

            pt = np.transpose(np.array([z,\
                                        R*np.sin(az_rad)*np.cos(elev_rad),\
                                        R*np.cos(az_rad)*np.cos(elev_rad)]))

            pti = np.concatenate((pti,pt),axis=0)

        pti = np.reshape(pti[1:], (nBeams,len(z),3))

        ds = xr.Dataset({'z': (['BeamIndex','AlongBeam'],pti[:,:,0]), \
                         'y': (['BeamIndex','AlongBeam'],pti[:,:,1]), \
                         'x': (['BeamIndex','AlongBeam'],pti[:,:,2]), \
                         'az': (['BeamIndex'], scanpoints.az.values), \
                         'elev': (['BeamIndex'], scanpoints.elev.values), \
                         'radius':(['BeamIndex','AlongBeam'],np.sqrt(pti[:,:,0]**2+pti[:,:,1]**2+pti[:,:,2]**2)) })

        return ds
    
    def RWF(self,z,z0):
        """Dynamically settable RWF function associated with the virtual lidar"""
        return self.RWF_func(z,z0)
    
    @staticmethod
    def none_RWF(z,z0):
        """A 'non' RWF which just takes the point value at the range gate"""
        
        import numpy as np
        
        rwf = np.zeros(len(z))
        assert (z[0]-z0)*(z[-1]-z0) <= 0

        idx = np.argmin(np.abs(z0-z))
        if(z[idx]>z0): idx = idx - 1

        rwf[idx]  = (z[idx+1]-z0)/(z[idx+1]-z[idx])
        rwf[idx+1]= (z0-z[idx])/(z[idx+1]-z[idx])

        return rwf
    
    @staticmethod
    def pulsed_RWF(z,z0,taum,tau):
        """
        RWF for a pulsed lidar: convolution of top hat range gate indicator function and gaussian pulse (Cariou and Boquet, 2010)
        
        Input:
        z -- array of radii along beam at whcih to evaluate the RWF [m]
        z0 -- range gate center [m]
        taum -- range gate [ns]
        tau -- FWHM of gaussian pulse [ns]
        """
        
        import numpy as np
        from scipy.special import erf

        c = 0.29979           # speed of light [m/ns]
        
        sqrtLn2 = np.sqrt(np.log(2))

        return 1./(c*taum)*(erf(4*sqrtLn2*(z-z0)/(c*tau)+sqrtLn2*taum/tau)
                          - erf(4*sqrtLn2*(z-z0)/(c*tau)-sqrtLn2*taum/tau) )
    
    def continuous_wave_RWF(z,z0,alpha0,lambdaw):
        """
        RWF for a continuous wave lidar: (Forsting, 2017)
        
        Input:
        z -- array of radii along beam at whcih to evaluate the RWF [m]
        z0 -- range gate center [m]
        alpha0 -- effective telescope radius [m]
        lambdaw -- laser wavelength [m]
        """
        
        import numpy as np

        zR = lambdaw*z0**2/np.pi/alpha0**2

        return zR/(zR**2+(z-z0)**2)/np.pi

    def check_RWF(self):
        
        from inspect import signature
        assert callable(self.RWF_func)
        str(signature(self.RWF_func)) == '(z, z0)'
        
        if self.RWF_func == self.pulsed_RWF:
            assert 'tau' in self.__dict__.keys()
            assert 'taum' in self.__dict__.keys()
            assert type(self.tau) in [int,float]
            assert type(self.taum) in [int,float]
            
        if self.RWF_func == self.continuous_wave_RWF:
            assert 'alpha0' in self.__dict__.keys()
            assert 'lambdaw' in self.__dict__.keys()
            assert type(self.alpha0) in [int,float]
            assert type(self.lambdaw) in [int,float]
    
    def apply_RWF(self,dsscan):
        """Convolve the LoS velocities along each beam with the set RWF to find averaged values at each range gate"""
        
        import numpy as np
        import xarray as xr

        scanpoints = self.get_scan_points()
        rwf_points = self.get_RWF_points()

        RWF_LoS = np.nan*np.zeros((len(dsscan.Time),self.ngates))
        for t in dsscan.Time.values:
            b = dsscan.BeamIndex[t].values

            range_gates = scanpoints.radius.isel(BeamIndex=b).values

            si = rwf_points.radius.isel(BeamIndex=b).values
            hi = si[1]-si[0] # assume uniform for now
            hi = (np.pad(si[1:]-si[:-1],(0,1)) + np.pad(si[1:]-si[:-1],(1,0)))/2.
            hi[0]  = hi[0]*2
            hi[-1] = hi[-1]*2

            vr_beam = dsscan.LoS.isel(Time=t).values
            maxr = len(si) - 1 - np.isfinite(vr_beam[::-1]).argmax()

            si[maxr:] = np.nan

            for r in range(len(range_gates)):
                tot = np.nansum(self.RWF(si,range_gates[r])*hi)

                if tot > 0.75: ## can change threshold for valid weights
                    RWF_LoS[t,r] = np.nansum(self.RWF(si,range_gates[r])*vr_beam*hi)/tot

        ds = xr.Dataset({'LoS':(["Time","AlongBeam"], RWF_LoS),\
                         'BeamIndex':(["Time"], dsscan.BeamIndex.values)},\
                         coords={'Time':dsscan.Time.values})

        return ds

class virtualScanningLidar(virtualLidar):
    """This is a class for scanning style lidars, their relevant parameters and scanning styles"""
    
    
    def __init__(self,lidartype='HaloStreamLine',**kwargs):
        
        # TODO: add default options

        self.name = lidartype
                
        super().__init__(**kwargs)
            
            
    def __str__(self):
        return "Scanning Lidar\n" + super().__str__()

    def check_scan(self):

       # TODO: any checks needed on scanning pattern?

       return


    def compute_scan_points(self,az,elev):
        
        import numpy as np
        import xarray as xr
        
        pti = np.array([[np.nan,np.nan,np.nan]])
        for b in range(len(az)):
            elev_rad = np.deg2rad(elev[b])
            az_rad = np.deg2rad(az[b])
            R = np.arange(0,self.ngates)*self.range_gate_res + self.minrange

            pt = np.transpose(np.array([R*np.sin(elev_rad),\
                                        R*np.sin(az_rad)*np.cos(elev_rad),\
                                        R*np.cos(az_rad)*np.cos(elev_rad)]))

            pti = np.concatenate((pti,pt),axis=0)
            
        pti = np.reshape(pti[1:], (len(az),len(R),3))
            
        ds = xr.Dataset({'z': (['BeamIndex','AlongBeam'],pti[:,:,0]), \
                         'y': (['BeamIndex','AlongBeam'],pti[:,:,1]), \
                         'x': (['BeamIndex','AlongBeam'],pti[:,:,2]), \
                         'az': (['BeamIndex'],az), 'elev': (['BeamIndex'], elev), \
                         'radius':(['BeamIndex','AlongBeam'],np.sqrt(pti[:,:,0]**2+pti[:,:,1]**2+pti[:,:,2]**2)) })

        self.scanpoints = ds
    
    def set_common_scan(self,scantype,**kwargs):
        
        import re
        import numpy as np
        import xarray as xr
        from scipy.signal import sawtooth
        
        setattr(self,'scantype',scantype)
                
        if self.scantype == 'stare':
            az, elev = [kwargs['az']], [kwargs['elev']]
            
            self.scandescription = 'Stare scan at %.2f az %.2f elev'%(kwargs['az'],kwargs['elev'])
            
        if self.scantype == 'RHI':
            amp = np.abs(kwargs['elev1']-kwargs['elev2'])/2.
            mid = (kwargs['elev1']+kwargs['elev2'])/2.
            
            ts = 2.*np.pi*np.linspace(0,1,2*kwargs['nintervals'])
    
            az, elev = kwargs['az']*np.ones(len(ts)), -amp*sawtooth(ts,width=0.5)+mid
        
            self.scandescription = 'RHI scan at %.2f az %.2f-%.2f elev %d intervals'%(kwargs['az'],kwargs['elev1'],kwargs['elev2'],kwargs['nintervals'])
        
        elif self.scantype == 'VAD':
            ts = 360.*np.linspace(0,1,kwargs['nintervals']+1)[:-1]
            
            az, elev = kwargs['az0']+ts, kwargs['elev']*np.ones(len(ts))
            
            self.scandescription = 'VAD scan at %.2f az0 %.2f elev %d intervals'%(kwargs['az0'],kwargs['elev'],kwargs['nintervals'])
            
        elif self.scantype == 'PPI':
            amp = np.abs(kwargs['az1']-kwargs['az2'])/2.
            mid = (kwargs['az1']+kwargs['az2'])/2.
            
            ts = 2.*np.pi*np.linspace(0,1,2*kwargs['nintervals'])
    
            az, elev = -amp*sawtooth(ts,width=0.5)+mid, kwargs['elev']*np.ones(len(ts))
        
            self.scandescription = 'PPI scan at %.2f-%.2f az %.2f elev %d intervals'%(kwargs['az1'],kwargs['az2'],kwargs['elev'],kwargs['nintervals'])
            
        else:
            raise ValueError('Scantype %s not recognized'%self.scantype)
            
        
        self.compute_scan_points(az,elev)

    def set_custom_scan(self,scantype,az,elev):
        
        assert len(az) == len(elev), 'Length of azimuthal and elevation angle lists do not match.'
        
        self.compute_scan_points(az,elev)

class virtualProfilingLidar(virtualLidar):
    """This is a class for profiling style lidars, their relevant parameters and DBS, VAD, or stare scanning styles"""

    reconstruct_vert_func = 'None'
    
    def __init__(self,lidartype='Windcubev1',**kwargs):
        
        self.name = lidartype
        
        ## will need to add temporal / accumulation time parameters
        if lidartype == 'Windcubev1':
            
            self.range_gate_res = 20.
            self.minrange = 40.
            self.ngates = 10
            
            self.RWF_func = self.pulsed_RWF
            self.tau = 265
            self.taum = 165
            
            self.scantype = 'DBS4'
            self.azimuth0 = 90.
            self.elevation = 62.
                        
            self.reconstruct_vert_func = self.reconstruct_vert_WCV1
            self.set_range_gates(self.minrange,self.ngates,self.range_gate_res,'pulsed')
            
        elif lidartype == 'Windcubev2':
            
            self.range_gate_res = 20.
            self.minrange = 40.
            self.ngates = 10
            
            self.RWF_func = self.pulsed_RWF
            self.tau = 265
            self.taum = 165
            
            self.scantype = 'DBSV4'
            self.azimuth0 = 90.
            self.elevation = 62.
            
            self.reconstruct_vert_func = self.reconstruct_vert_WCV2_wdir_weight
            self.set_range_gates(self.minrange,self.ngates,self.range_gate_res,'pulsed')
            
        elif lidartype == 'ZephIR300':
            
            self.range_gate_res = 20.
            self.minrange = 20.
            self.ngates = 10
            
            self.RWF_func = self.continuous_wave_RWF
            self.alpha0 = 28e-3
            self.lambdaw = 1565e-9
            
            self.scantype = 'DBS50' ##50 at 1s rev or 150 at 3s rev
            self.azimuth0 = 0.
            self.elevation = 60.
            
            self.reconstruct_vert_func = self.reconstruct_vert_lstsq_fit
            self.set_range_gates(self.minrange,self.ngates,self.range_gate_res,'cw')

        for k in kwargs.keys():
            setattr(self, k, kwargs[k])

        super().__init__(**kwargs)
        
    def __str__(self):
        
        printstr = "Profiling Lidar\n" + super().__str__()
        
        if callable(self.reconstruct_vert_func):
            printstr = printstr + "\nReconstruction:        %s"%(self.reconstruct_vert_func.__name__)
        else:
            printstr = printstr + "\nReconstruction:        %s"%(self.reconstruct_vert_func)
            
        return printstr
    
    def check_scan(self):

        import numpy as np
        
        scanpoints = self.get_scan_points()
        
        if self.reconstruct_vert_func in [self.reconstruct_vert_WCV2_nowdir,self.reconstruct_vert_WCV2_wdir_weight]:
            
            assert len(scanpoints.BeamIndex) == 5
            assert np.max( np.abs(scanpoints.az.values[1:-1]-scanpoints.az.values[:-2]) - np.array([90.,90.,90.]) ) < 1e-14, 'Scan points azimuths do not match DBSV4 scan, cannot reconstruct with chosen function'
            assert np.max(np.abs(scanpoints.elev.values[1:-1]-scanpoints.elev.values[0])) <1e-14 and scanpoints.elev.values[4]==90, 'Scan points elevations do not match DBSV4 scan, cannot reconstruct with chosen function'

        elif self.reconstruct_vert_func == self.reconstruct_vert_WCV1:
            assert len(scanpoints.BeamIndex) == 4
            assert np.max( np.abs(scanpoints.az.values[1:]-scanpoints.az.values[:-1]) - np.array([90.,90.,90.]) ) < 1e-14, 'Scan points azimuths do not match DBS4 scan, cannot reconstruct with chosen function'
            assert np.max(np.abs(scanpoints.elev.values[1:]-scanpoints.elev.values[0])) <1e-14, 'Scan points elevations do not match DBS4 scan, cannot reconstruct with chosen function'
            
        
#         elif self.reconstruct_vert_func = self.reconstruct_vert_lstsq_fit # no requirements on lstsq fit?
    
    def compute_scan_points(self,az,elev):
        
        import numpy as np
        import xarray as xr
        
        pti = np.array([[np.nan,np.nan,np.nan]])
        for b in range(len(az)):
            elev_rad = np.deg2rad(elev[b])
            az_rad = np.deg2rad(az[b])
            z = np.arange(0,self.ngates)*self.range_gate_res + self.minrange

            pt = np.transpose(np.array([z,\
                                        z*np.sin(az_rad)/np.tan(elev_rad),\
                                        z*np.cos(az_rad)/np.tan(elev_rad)]))
            
            pti = np.concatenate((pti,pt),axis=0)

        pti = np.reshape(pti[1:], (len(az),len(z),3))

        ds = xr.Dataset({'z': (['BeamIndex','AlongBeam'],pti[:,:,0]),\
                         'y': (['BeamIndex','AlongBeam'],pti[:,:,1]),\
                         'x': (['BeamIndex','AlongBeam'],pti[:,:,2]),\
                         'az': (['BeamIndex'],az), 'elev': (['BeamIndex'], elev),\
                         'radius':(['BeamIndex','AlongBeam'],np.sqrt(pti[:,:,0]**2+pti[:,:,1]**2+pti[:,:,2]**2)) })

        self.scanpoints = ds
    
    def set_common_scan(self,scantype,**kwargs):
        
        import re
        import numpy as np
        import xarray as xr
        
        setattr(self,'scantype',scantype)
        
        if 'azimuth0' in self.__dict__.keys() and 'az0' not in kwargs.keys():
            kwargs['az0'] = self.azimuth0
        
        if 'elevation' in self.__dict__.keys() and 'elev' not in kwargs.keys():
            kwargs['elev'] = self.elevation

        if self.scantype == 'stare':
            assert 'az' in kwargs.keys() and 'elev' in kwargs.keys(), "Need azimuthal and elevation angle for stare scan; use set_common_scan('stare',az=XX,elev=XX)"
            az, elev = [kwargs['az']], [kwargs['elev']]
            
            self.scandescription = 'Stare scan at %.2f az %.2f elev'%(kwargs['az'],kwargs['elev'])
            
        elif self.scantype[:4] == 'DBSV':

            p = re.compile('DBSV[0-9]+')
            if len(p.findall(self.scantype)) > 0:
                nagle = int(p.findall(self.scantype)[0][4:])
            else:
                nagle = 4
                
            az, elev = kwargs['az0']-np.linspace(0,360,nagle+1), np.append(kwargs['elev']*np.ones(nagle),[90.])
            
            self.scandescription = 'DBSV%d with vertical scan at %.2f az %.2f elev'%(nagle,kwargs['az0'],kwargs['elev'])

        elif self.scantype[:3] == 'DBS':

            p = re.compile('DBS[0-9]+')
            if len(p.findall(self.scantype)) > 0:
                nagle = int(p.findall(self.scantype)[0][3:])
            else:
                nagle = 4
                
            az, elev = kwargs['az0']-np.linspace(0,360,nagle+1)[:-1], kwargs['elev']*np.ones(nagle)
            
            self.scandescription = 'DBS%d scan at %.2f az %.2f elev'%(nagle,kwargs['az0'],kwargs['elev'])
            
        elif self.scantype == 'VAD':
            ts = 360.*np.linspace(0,1,kwargs['nintervals']+1)[:-1]
            
            az, elev = kwargs['az0']+ts, kwargs['elev']*np.ones(len(ts))
            
            self.scandescription = 'VAD scan at %.2f az0 %.2f elev with %d points'%(kwargs['az0'],kwargs['elev'],kwargs['nintervals'])
            
        else:
            raise ValueError('Scantype %s not recognized'%self.scantype)
            
        self.compute_scan_points(az,elev)

        
    def set_custom_scan(self,scantype,az,elev):
        
        assert len(az) == len(elev), 'Length of azimuthal and elevation angle lists do not match.'
        
        self.compute_scan_points(az,elev)
        
    def set_reconstruction(self,reconstruction_type,reconst_func=None):
        
        if reconst_func is None:
            reconstruct_functions = {'None':self.reconstruct_none,                                     'WCV1':self.reconstruct_vert_WCV1,                                     'WCV2':self.reconstruct_vert_WCV2_nowdir,                                     'WCV2_wdir':self.reconstruct_vert_WCV2_wdir_weight,                                     'LstSq': self.reconstruct_vert_lstsq_fit}

            assert reconstruction_type in reconstruct_functions.keys(), 'Reconstruction type not recognized.'
            self.reconstruct_vert_func = reconstruct_functions[reconstruction_type]
        
        else:
            self.reconstruct_vert_func = reconst_func
    
    def reconstruct_vert_prof(self,dsscan):
        return self.reconstruct_vert_func(dsscan,self.get_scan_points())
                              
    @staticmethod
    def reconstruct_none(dsscan,dscanpoints):
        return dsscan
    
    @staticmethod
    def reconstruct_vert_WCV1(dsscan,dscanpoints):
        """
        Standard function for a version 1 Windcube to solve for 3-D velocities based on horizontal homogeneity assumption.
        The 4 beams are assumed to increment clockwise by 90 degrees, North (+V velocity) is set by the first beam azimuth

        Arguments:
        dsscan:     dataset with line of sight velocities on [N,E,S,W,V] beams at range gate locations (same height)
        scanpoints: points describing the scan pattern/points used. Should be lidar.get_scan_points() as used for scan
        """

        import numpy as np
        import xarray as xr

        elev = scanpoints.elev.values[0]

        assert len(scanpoints.BeamIndex) == 4
        assert np.max( np.abs(scanpoints.az.values[1:]-scanpoints.az.values[:-1]) - np.array([90.,90.,90.]) ) < 1e-14, 'Scan points azimuths do not match DBS4 scan, cannot reconstruct with chosen function'
        assert np.max(np.abs(scanpoints.elev.values-elev)) <1e-14, 'Scan points elevations do not match DBS4 scan, cannot reconstruct with chosen function'

        dat = dsscan
        elev = np.deg2rad(elev)
        az = np.deg2rad(dat.az.values)

        E = dat.isel(BeamIndex=0).LoS.values
        N = dat.isel(BeamIndex=1).LoS.values
        W = dat.isel(BeamIndex=2).LoS.values
        S = dat.isel(BeamIndex=3).LoS.values

        u_est = (E-W)/(2.*np.cos(elev))
        v_est = (N-S)/(2.*np.cos(elev))

        w_est = (E+N+W+S)/(4.*np.sin(elev))

        dat['uest'] = xr.DataArray(u_est, dims=['Time','AlongBeam'])
        dat['vest'] = xr.DataArray(v_est, dims=['Time','AlongBeam'])
        dat['west'] = xr.DataArray(w_est, dims=['Time','AlongBeam'])

        return dat

    @staticmethod
    def reconstruct_vert_WCV2_nowdir(dsscan,scanpoints):
        """
        Solve for 3-D velocities based on horizontal homogeneity assumption across lidar sample volume.
        The first 4 beams are assumed to increment clockwise by 90 degrees at fixed elevation angle followed by a vertical beam
        North (+V velocity) is set by the first beam azimuth

        Arguments:
        dsscan:     dataset with line of sight velocities on [N,E,S,W,V] beams at range gate locations (same height)
        scanpoints: points describing the scan pattern/points used. Should be lidar.get_scan_points() as used for scan
        """

        import numpy as np
        import xarray as xr

        elev = scanpoints.elev.values[0]
        
        assert len(scanpoints.BeamIndex) == 5
        assert np.max( np.abs(scanpoints.az.values[1:-1]-scanpoints.az.values[:-2]) - np.array([90.,90.,90.]) ) < 1e-14, 'Scan points azimuths do not match DBSV4 scan, cannot reconstruct with chosen function'
        assert np.max(np.abs(scanpoints.elev.values[1:-1]-scanpoints.elev.values[0])) <1e-14 and scanpoints.elev.values[4]==90, 'Scan points elevations do not match DBSV4 scan, cannot reconstruct with chosen function'
        
        elev = np.deg2rad(elev)
        
        u_est = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        v_est = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        w_est = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))

        N = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        E = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        S = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        W = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))

        N[0:] = np.repeat(dsscan.LoS[0::5].values,5,axis=0)[0:len(dsscan.Time.values)  ,:]
        E[1:] = np.repeat(dsscan.LoS[1::5].values,5,axis=0)[0:len(dsscan.Time.values)-1,:]
        S[2:] = np.repeat(dsscan.LoS[2::5].values,5,axis=0)[0:len(dsscan.Time.values)-2,:]
        W[3:] = np.repeat(dsscan.LoS[3::5].values,5,axis=0)[0:len(dsscan.Time.values)-3,:]

        V = dsscan.LoS[4::5].values

        u_est = (E-W)/(2*np.cos(elev))
        v_est = (N-S)/(2*np.cos(elev))

        w_est = (N+S+E+W)/(4.*np.sin(elev))
        w_est[4::5,:] = V
        
        dsscan['u_est'] = xr.DataArray(u_est, dims=['Time','AlongBeam'])
        dsscan['v_est'] = xr.DataArray(v_est, dims=['Time','AlongBeam'])
        dsscan['w_est'] = xr.DataArray(w_est, dims=['Time','AlongBeam'])

        return dsscan
    
    @staticmethod
    def reconstruct_vert_WCV2_wdir_weight(dsscan,scanpoints):
        """
        Solve for 3-D velocities based on horizontal homogeneity assumption and updated weighting by wind direction.
        The first 4 beams are assumed to increment clockwise by 90 degrees at fixed elevation angle followed by a vertical beam
        North (+V velocity) is set by the first beam azimuth

        Arguments:
        dsscan:     dataset with line of sight velocities on [N,E,S,W,V] beams at range gate locations (same height)
        scanpoints: points describing the scan pattern/points used. Should be lidar.get_scan_points() as used for scan
        """
        
        import numpy as np
        import xarray as xr
        
        elev = scanpoints.elev.values[0]

        assert len(scanpoints.BeamIndex) == 5
        assert np.max( np.abs(scanpoints.az.values[1:-1]-scanpoints.az.values[:-2]) - np.array([90.,90.,90.]) ) < 1e-14, 'Scan points azimuths do not match DBSV4 scan, cannot reconstruct with chosen function'
        assert np.max(np.abs(scanpoints.elev.values[1:-1]-scanpoints.elev.values[0])) <1e-14 and scanpoints.elev.values[4]==90, 'Scan points elevations do not match DBSV4 scan, cannot reconstruct with chosen function'
            
        elev = np.deg2rad(elev)

        u_est = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        v_est = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        w_est = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))

        N = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        E = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        S = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        W = np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))

        N[0:] = np.repeat(dsscan.LoS[0::5].values,5,axis=0)[0:len(dsscan.Time.values)  ,:]
        E[1:] = np.repeat(dsscan.LoS[1::5].values,5,axis=0)[0:len(dsscan.Time.values)-1,:]
        S[2:] = np.repeat(dsscan.LoS[2::5].values,5,axis=0)[0:len(dsscan.Time.values)-2,:]
        W[3:] = np.repeat(dsscan.LoS[3::5].values,5,axis=0)[0:len(dsscan.Time.values)-3,:]

        V = dsscan.LoS[4::5].values

        u_est = (E-W)/(2*np.cos(elev))
        v_est = (N-S)/(2*np.cos(elev))

        wdir = -np.arctan(v_est/(u_est+1e-14))+np.pi/2.
        wdir[np.where(u_est <= 0.)] = wdir[np.where(u_est <= 0.)]+np.pi
        wdir[np.where(wdir<0.)] = wdir[np.where(wdir<0.)]+2*np.pi

        w_est = ( (N+S)*np.cos(wdir)**2 + (E+W)*np.sin(wdir)**2 )/(2.*np.sin(elev))
        w_est[4::5,:] = V

        dsscan['u_est'] = xr.DataArray(u_est, dims=['Time','AlongBeam'])
        dsscan['v_est'] = xr.DataArray(v_est, dims=['Time','AlongBeam'])
        dsscan['w_est'] = xr.DataArray(w_est, dims=['Time','AlongBeam'])

        return dsscan
    
    @staticmethod
    def reconstruct_vert_lstsq_fit(dsscan,scanpoints):
        """
        Solve for 3-D velocities based on horizontal homogeneity assumption and updated weighting by wind direction.

        Arguments:
        dsscan:     dataset with line of sight velocities on [N,E,S,W,V] beams at range gate locations (same height)
        scanpoints: points describing the scan pattern/points used. Should be lidar.get_scan_points() as used for scan
        """
                
        import numpy as np
        import xarray as xr
        from numpy.linalg import lstsq
                
        nbeams = len(scanpoints.BeamIndex)
        ntime = len(dsscan.Time)
        
        B = np.empty((nbeams,3))
        
        for b in range(nbeams):
            az = np.deg2rad(scanpoints.az.values[b])
            elev = np.deg2rad(scanpoints.elev.values[b])
            B[b,:] = [np.cos(elev)*np.cos(az),np.cos(elev)*np.sin(az),np.sin(elev)]
        
        u_est = np.nan*np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        v_est = np.nan*np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        w_est = np.nan*np.zeros((len(dsscan.Time),len(dsscan.AlongBeam)))
        
        for t in range(0,ntime-nbeams+1):
            u, _, _, _ = lstsq(np.roll(B,-t,axis=0),dsscan.LoS.values[t:t+nbeams,:],rcond=None)
            
            u_est[t+nbeams-1,:] = u[0]
            v_est[t+nbeams-1,:] = u[1]
            w_est[t+nbeams-1,:] = u[2]

        dsscan['u_est'] = xr.DataArray(u_est, dims=['Time','AlongBeam'])
        dsscan['v_est'] = xr.DataArray(v_est, dims=['Time','AlongBeam'])
        dsscan['w_est'] = xr.DataArray(w_est, dims=['Time','AlongBeam'])

        return dsscan

def get_weights(fullscanpoints, xypos, lesfile):
    
    import numpy as np
    import xarray as xr
    import scipy.interpolate as spint
    import scipy.spatial.qhull as qhull
    
    def interp_weights(xyz, uvw,d=3):
        tri = qhull.Delaunay(xyz)
        simplex = tri.find_simplex(uvw)
        vertices = np.take(tri.simplices, simplex, axis=0)
        temp = np.take(tri.transform, simplex, axis=0)
        delta = uvw - temp[:, d]
        bary = np.einsum('njk,nk->nj', temp[:, :d, :], delta)
        return vertices, np.hstack((bary, 1 - bary.sum(axis=1, keepdims=True)))
    
    dsin = xr.open_dataset(lesfile)
        
    nbeams = len(fullscanpoints.BeamIndex)

    nx = len(dsin.west_east)
    ny = len(dsin.south_north)
    nz = len(dsin.bottom_top)
    dx = dsin.attrs['DX']
    dy = dsin.attrs['DY']

    Xtot, Ytot = np.meshgrid( np.arange(0,nx)*dx, np.arange(0,ny)*dy )
    Xtot = np.tile(Xtot,(nz,1,1)) - xypos[0]*dx
    Ytot = np.tile(Ytot,(nz,1,1)) - xypos[1]*dy
    
    Ztot = ( (dsin.PH.values+dsin.PHB.values)[0,1:,:,:]+(dsin.PH.values+dsin.PHB.values)[0,:-1,:,:] )/(2*9.81)- xypos[2] 
    
    vtx = []
    wts = []
    idx = []

    for beam in range(nbeams):

        scanpoints = fullscanpoints.isel(BeamIndex=beam)
        pti = np.transpose(np.array([scanpoints.x.values.flatten(),\
                                     scanpoints.y.values.flatten(),\
                                     scanpoints.z.values.flatten()]))

        pti = pti[~np.isnan(pti).any(axis=1)]
    
        i = (pti[:,0]/dx+xypos[0]).astype('int16')
        j = (pti[:,1]/dy+xypos[1]).astype('int16')

        k = np.array([np.argmin(np.abs(Ztot[:,j[p],i[p]]-pti[p,2])) for p in range(np.shape(pti)[0])])

        ############

        i = np.concatenate([i-2,i-1,i,i+1,i+2,\
                            i  ,i  ,i,i  ,i  ,\
                            i  ,i  ,i,i  ,i  ])

        j = np.concatenate([j  ,j  ,j,j  ,j  ,\
                            j-2,j-1,j,j+1,j+2,\
                            j  ,j  ,j,j  ,j  ])

        k = np.concatenate([k  ,k  ,k,k  ,k  ,\
                            k  ,k  ,k,k  ,k  ,\
                            k-2,k-1,k,k+1,k+2])

        idx_b = np.unique(np.array([i,j,k]).T,axis=0)

        X = Xtot[idx_b[:,2],idx_b[:,1],idx_b[:,0]]
        Y = Ytot[idx_b[:,2],idx_b[:,1],idx_b[:,0]]
        Z = Ztot[idx_b[:,2],idx_b[:,1],idx_b[:,0]]

        pts = np.transpose(np.array([X.flatten(),Y.flatten(),Z.flatten()]))

        vtx_b, wts_b = interp_weights(pts, pti)
        
        vtx.append(vtx_b)
        wts.append(wts_b)
        idx.append(idx_b)
    
    dsin.close()
    
    return vtx, wts, idx

def collect_timestaggered_lidar_beam_velocities(lidar,xypos,flist,RWF=True,regulargrid=False,staticgrid=False):
    """
    Driver function to run offline virtual lidar scan on WRFLES output data
    ++ guidance on input
    
    Arguments:
    lidar: virtualLidar object describing the instrument (need to have scan parameters pre-set)
    xypos: position of the instrument [x,y,(elev)] where elevation (m) is optional. If omitted will take elev=0
    flist: wrf output files or auxhist files containing minimum of (U,V,W,PH,PHB) CHECK
    regulargrid: whether the LES grid is regular, rectilinear grid
    staticgrid: whether the LES grid evolves with time
    """
    
    import numpy as np
    import xarray as xr
        
    import scipy.interpolate as spint
    import scipy.spatial.qhull as qhull
    
    import time

    def interpolate(values, vtx, wts):
        return np.einsum('nj,nj->n', np.take(values, vtx), wts)
    
    ds = xr.open_dataset(flist[0])
    terr = ds.HGT[0,:,:]
    DX = ds.DX
    DY = ds.DY
    NX = len(ds.west_east)
    NY = len(ds.south_north)
    ds.close()
    
    ## Check for position arguments and extract elevation of lidar from terrain if not input
    assert len(xypos) >= 2

    if len(xypos) == 2:
        xypos.append( terr[xypos[1],xypos[0]].values )
        print("At %f, %f WRF Terrain height: %.2fm\n"%(xypos[0],xypos[1],xypos[2]))

    elif len(xypos) > 2:
        print("Input elevation: %.2fm\n"%(xypos[2]))  
    
    if RWF:
        fullscanpoints = lidar.get_RWF_points() 
    else:
        fullscanpoints = lidar.get_scan_points() 
        
    nbeams = len(fullscanpoints.BeamIndex)
    ntimes = len(flist)
    nalongbeam = len(fullscanpoints.AlongBeam)

    # filter points blocked by terrain
    fter = spint.interp2d(DY*(np.arange(NY)-xypos[1]),\
                          DX*(np.arange(NX)-xypos[0]),\
                          terr, kind='cubic')

    for b in range(nbeams):
        for i in range(nalongbeam):
            if fullscanpoints.z.values[b,i]+xypos[2] <= fter(fullscanpoints.y.values[b,i],fullscanpoints.x[b,i]):
                fullscanpoints.x.values[b,i-1:] = np.nan
                fullscanpoints.y.values[b,i-1:] = np.nan
                fullscanpoints.z.values[b,i-1:] = np.nan
                fullscanpoints.radius.values[b,i-1:] = np.nan
                break
    
    ds = xr.Dataset({'uvel':(["Time","AlongBeam"], np.nan*np.ones((ntimes,nalongbeam))),\
                     'vvel':(["Time","AlongBeam"], np.nan*np.ones((ntimes,nalongbeam))),\
                     'wvel':(["Time","AlongBeam"], np.nan*np.ones((ntimes,nalongbeam))),\
                     'LoS':(["Time","AlongBeam"],  np.nan*np.ones((ntimes,nalongbeam))),\
                     'BeamIndex':(["Time"], np.arange(ntimes)%nbeams)},coords={'Time':np.arange(ntimes)})
    
    vtx, wts, idx = get_weights_fast(fullscanpoints,xypos,flist[0])
    
    for f in range(len(flist)):
        
        dsin = xr.open_dataset(flist[f])
        t = 0

        print("Processing Time: %2d"%f)

        beam = f%nbeams

        scanpoints = fullscanpoints.isel(BeamIndex=((beam)))
        pti = np.transpose(np.array([scanpoints.x.values.flatten(),\
                                     scanpoints.y.values.flatten(),\
                                     scanpoints.z.values.flatten()]))

        az = np.deg2rad(scanpoints.az.values)
        elev = np.deg2rad(scanpoints.elev.values)

        uvel =  ( (dsin.U.values[t,:,:,1:]+dsin.U.values[t,:,:,:-1])/2. )[idx[beam][:,2],idx[beam][:,1],idx[beam][:,0]]
        vvel =  ( (dsin.V.values[t,:,1:,:]+dsin.V.values[t,:,:-1,:])/2. )[idx[beam][:,2],idx[beam][:,1],idx[beam][:,0]]
        wvel =  ( (dsin.W.values[t,1:,:,:]+dsin.W.values[t,:-1,:,:])/2. )[idx[beam][:,2],idx[beam][:,1],idx[beam][:,0]]

        uinterp = interpolate(uvel.flatten(), vtx[beam], wts[beam])
        vinterp = interpolate(vvel.flatten(), vtx[beam], wts[beam])
        winterp = interpolate(wvel.flatten(), vtx[beam], wts[beam])

        ds.uvel.values[f,:len(uinterp)] = uinterp
        ds.vvel.values[f,:len(vinterp)] = vinterp
        ds.wvel.values[f,:len(winterp)] = winterp

        ds.LoS.values[f,:len(uinterp)]  = uinterp*np.cos(az)*np.cos(elev) + \
                                          vinterp*np.sin(az)*np.cos(elev) + \
                                          winterp*np.sin(elev)

        dsin.close()
    
    return ds

def process_beam_velocity_data(lidar,ds,save_dir=None,reconstruct=True,RWF=True):
    """
    Perform the post-processing on the raw LoS velocities collected along each of the scan beams.
    That includes: range gate weighting centered on range gates and 3D velocity reconstructions.
    The specific functions may be set as part of the virtual lidar object configuration. This routine
        may be used to easily rerun a virtual scan from the raw velocity data with different processing.


    Arguments:
    lidar:        (virtual lidar object) configured with the desired settings
    ds:           (xarray dataset) raw beam velocity dataset, corresponding to scan in the lidar object
    reconstruct:  (boolean), whether to perform a 3D velocity reconstruction
    RWF:          (boolean), whether to perform range gate weighting (vs direct interpolated sample)
    save_dir:     (string) directory to write a batch of output files with end and intermediate results (optional)
    """
    
    if save_dir is not None and save_dir[-1] != '/':
        save_dir = save_dir + '/'

    if RWF:
        if save_dir is not None:
            lidar.get_RWF_points().to_netcdf(save_dir+'raw_points.nc')
            ds.to_netcdf(save_dir+'raw_beam_velocities.nc')
        
        ds = lidar.apply_RWF(ds)

    if reconstruct:
        if type(lidar).__name__ == 'virtualProfilingLidar' and 'reconstruct_vert_func' in lidar.__dict__.keys():
            ds = lidar.reconstruct_vert_prof(ds)
                
    if save_dir is not None:
        lidar.get_scan_points().to_netcdf(save_dir+'lidar_scan_points.nc')
        ds.to_netcdf(save_dir+'lidar_scan_output.nc')
        
    ## TODO: add attributes to the output files
            
    return ds


def run_lidar_scan(lidar,xypos,flist,save_dir=None,no_process=False):    
    """
    Run a full, complete scan. That includes collecting the velocities and applying all the processing.


    Arguments:
    lidar:        (virtual lidar object) configured with the desired settings
    xypos:        (tuple) position of the lidar in the LES domain (currently x,y cell coordinates) 
    flist:        (list) ordered list of output files (currently one per time, corresponding to beam frequency)
    save_dir:     (string) directory to write a batch of output files with end and intermediate results (optional)
    no_process:   (boolean), whether to perform the RWF and reconstruction processing or collect raw interpolated radial velocities
    """
        
    import os
    import copy
    
    lidar.check_RWF()
    lidar.check_scan()
    
    if save_dir is not None and save_dir[-1] != '/':
        save_dir = save_dir + '/'
        
    if save_dir is not None:
        assert os.path.isdir(save_dir), 'Given save directory does not exist'
        
    ds = collect_timestaggered_lidar_beam_velocities(lidar,xypos,flist)
    
    if save_dir is not None and type(lidar).__name__ == 'virtualProfilingLidar':
        true_stare = copy.deepcopy(lidar)
        true_stare.set_common_scan('stare',az=0,elev=90)
        dst = collect_timestaggered_lidar_beam_velocities(true_stare,xypos,flist,RWF=False)
        
        true_stare.get_scan_points().to_netcdf(save_dir+'true_vert_points.nc')
        dst.to_netcdf(save_dir+'true_vert_profile.nc')
            
    return process_beam_velocity_data(lidar,ds,save_dir=save_dir)

