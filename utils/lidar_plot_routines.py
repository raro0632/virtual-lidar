def get_wspd_wdir(u,v):
    
    import numpy as np
    import xarray as xr
    
    try:
        u = u.values.astype(float)
        v = v.values.astype(float)
    except:
        pass
    
    wspd = (u**2+v**2)**0.5
    
    wdir = np.rad2deg(np.arctan(u,v))
#     wdir = np.rad2deg(np.arccos(u/wspd))
#     idx = np.where(v < 0)
#     wdir[idx] = -wdir[idx] + 360.
    
    return wspd, wdir
        
def plot_3Dscan(scan,**kwargs):
    
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure(figsize=(6,6))
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter3D([0],[0],[0],marker='o',s=20,color='k')
    
    if 'values' in kwargs.keys():
        scat = ax.scatter3D(ds.x.values.flatten(),ds.y.values.flatten(),ds.z.values.flatten(),\
                    c=ds[values].values.flatten(),cmap=plt.cm.viridis)
        plt.colorbar(scat)
        
    else:
        for i in range(len(scan.BeamIndex)):
            ax.scatter3D(scan.x.values[i,:].flatten(),scan.y.values[i,:].flatten(),scan.z.values[i,:].flatten())
        
    ax.set_xlabel('West-East')
    ax.set_ylabel('South-North')
    ax.set_zlabel('Height')